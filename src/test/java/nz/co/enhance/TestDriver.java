package nz.co.enhance;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import nz.co.enhance.automationFramework.AutomationType;
import nz.co.enhance.automationFramework.Automator;
import nz.co.enhance.automationFramework.HelperClasses.PropertiesHandler;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Properties;

public class TestDriver {

    public static Automator automator;
    public static Properties properties;
    public String runTarget;

    public TestDriver() {
        setup();

    }

    public void setup() {

        //get the runtime properties - defines which target you will run your tests against.
        properties = new PropertiesHandler().loadProperties("src/main/resources/global.properties");

        runTarget = setEnvironmentProperty("runTarget");

        if (runTarget.equalsIgnoreCase("chrome")) {
            //automator = new Automator(AutomationType.CHROME);
            System.setProperty("webdriver.chrome.driver", "/Users/enhance/Documents/installers-for-mac/chrome driver/73.0.3683/chromedriver");
            automator = new Automator((AutomationType.CHROMESELFMANAGED));
            automator.driver.manage().window().maximize();
        }
        if (runTarget.equalsIgnoreCase("firefox")) {
            automator = new Automator(AutomationType.FIREFOX);
        }
        if (runTarget.equalsIgnoreCase("safari")) {
            automator = new Automator(AutomationType.SAFARI);
        }
        if (runTarget.equalsIgnoreCase("edge")) {
            automator = new Automator(AutomationType.EDGE);
        }
        if (runTarget.equalsIgnoreCase("ie")) {
            //You must follow these steps before IE will work:
            // https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver#required-configuration
            DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
            caps.setCapability("INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS", true);
            caps.setCapability("requireWindowFocus", true);
            automator = new Automator(AutomationType.IE, caps);
        }

        if (runTarget.equalsIgnoreCase("ios")) { //change or parameterise these to suit your requirements
            // This set of capabilities is designed for an emulator. Real device is different.
            // Note that if you add a UDID for an emulator it will prevent restarts between scenarios which is desirable
            // for speed reasons if you are executing multiple tests.
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("platformName", "iOS");
            caps.setCapability("platformVersion", "12.2");
            //caps.setCapability("deviceName", "iPhone XS Max");
            caps.setCapability("deviceName", "iPhone6PlusVersion12");
            //caps.setCapability("version", "12");
            caps.setCapability("version", "12.2");
            //caps.setCapability("udid", "24637D95-870A-4BAB-855A-8D127E21A025");
            caps.setCapability("udid", "0D5A4474-9B8F-4708-B64D-059B627F60E6");
            caps.setCapability("automationName", "XCUITest");
            caps.setCapability("browserName", "Safari");
            caps.setCapability("noReset", true);
            caps.setCapability("fullReset", false);
            caps.setCapability("newCommandTimeout", 999);
            automator = new Automator(AutomationType.IOS, caps);
        }

        if (runTarget.equalsIgnoreCase("android")) { //change or parameterise these to suit your requirements
            // This set of capabilities is designed for an emulator. Real device is slightly different.

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("platformName", "android");
            caps.setCapability("deviceName", "New Device API 28");
            caps.setCapability("browserName", "Chrome");
            caps.setCapability("automationName", "UiAutomator2");

            //We use different versions of Chromedriver for mobile
            caps.setCapability("chromedriverUseSystemExecutable", "false");
            caps.setCapability("chromedriverExecutableDir", "/Users/enhance/Documents/installers-for-mac/chrome driver/2.44_for_browser69/");

            caps.setCapability("newCommandTimeout", 999);
            automator = new Automator(AutomationType.ANDROID, caps);
        }
    }


    public String setEnvironmentProperty(String propertyName) {
        if (System.getenv().containsKey(propertyName)) {
            return System.getenv(propertyName);
        } else if (System.getenv().containsKey(propertyName.toUpperCase())) {   //some instances of windows are uppercasing runtarget
            return System.getenv(propertyName.toUpperCase());
        } else {
            return properties.getProperty(propertyName);
        }
    }

    public Boolean setEnvironmentProperty(String propertyName, boolean isBoolean) {
        if (System.getenv().containsKey(propertyName)) {
            return Boolean.valueOf(System.getenv(propertyName));
        } else {
            return Boolean.valueOf(properties.getProperty(propertyName));
        }
    }

    public void cleanup() {
        //if we need any logging or extra reporting we can pop it here.
        if (automator != null) {
            automator.quit();
        }
    }
}
