package nz.co.enhance.Objects.Examples;

import nz.co.enhance.automationFramework.MobElement;
import org.openqa.selenium.By;

public class NZHeraldPage {

    public MobElement homeButton = new MobElement(By.className("logo"));
    public MobElement newsPane = new MobElement(By.id("pb-root"));

    public NZHeraldPage(){
        newsPane.waitForElementToBeDisplayed(20);
    }

}
