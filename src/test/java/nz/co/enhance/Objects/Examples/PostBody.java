package nz.co.enhance.Objects.Examples;

import nz.co.enhance.automationFramework.HelperClasses.JSONParser;
import nz.co.enhance.automationFramework.HelperClasses.TextPrettifier;

import static nz.co.enhance.automationFramework.HelperClasses.APIHelper.objectToJson;

public class PostBody {
    public int userId;
    public String title;
    public String body;

    public String toJson() {
        return TextPrettifier.indentJson(new JSONParser(objectToJson(this)).toJsonString());
    }
}
