# API Exercise
#
# Create a set of several scenarios to test the endpoints detailed here: https://reqres.in/
# - Successful GET request
# - Unsuccessful GET request
# - Successful POST request using an object to model the posted data
# - Unsuccessful POST request using an object to model the posted data (hint - nulls in an object do not get converted to JSON!)
# - Successful DELETE request
# - Successful PUT request
# - Parse the responses using JSONParser to get accurate assertions.
# - Tag your Scenarios appropriately

# In order to create good tests make sure you do the following:
# - Generate reusable, concise step defs (see the guide to writing good cucumber)
# - Think about how to create requests in a reusable way and how to store endpoints.
# - Model request data in objects
# - Parse the responses properly rather than asserting against the entire response body as text.
# - Always check the response code
# - Ensure you use Junit assertions correctly and appropriately (all tests must have at least one assertion!)
# - Put incorrect data through your tests to be sure your assertions are working correctly.
#
# Helpful links:
#
# Guide to writing good Cucumber: https://saucelabs.com/blog/write-great-cucumber-tests


