@api
Feature: Examples of creating Rest requests and parsing JSON

  Scenario: Send a simple GET request and parse the response
    When I send a GET to the test api for user 2
    Then I see the response code is 200
    And I see the response contains the following data:
      | fieldName | expectedResult                     |
      | id        | 2                                  |
      | userId    | 1    caps.setCapability("browserName", "Chrome");                              |
      | title     | quis ut nam facilis et officia qui |
      | completed | false                              |

  Scenario: Create a POST request using objects
    When I send a POST request with the following data:
      | fieldName | value      |
      | userId    | 1          |
      | title     | Title text |
      | body      | Body text  |
    Then I see the response code is 201
    And I see the response contains the text id