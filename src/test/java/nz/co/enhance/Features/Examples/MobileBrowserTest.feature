@ios @android @mobile
Feature: Perform a web test on a mobile device

#  This test will run equally well on Android and Ios. Because it is a web-based test it uses the same locators on both.
#  You do need to change the global.properties to use ios or android and configure the capabilities in the TestDriver
#  to suit your environment. If doing this for a client with multiple configurations you would create desired capability
#  profiles and load the correct one at runtime.

  Scenario: Check the NZ Herald page loads correctly
    When I navigate to the URL http://www.nzherald.co.nz
    Then I see the NZ Herald home icon is visible