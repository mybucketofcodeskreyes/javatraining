@web
Feature: Simple web test examples using Google

  Perform simple search and verify actions on a google page.

  @search
  Scenario: Perform a google search for a specified term
    Given I navigate to the URL https://www.google.com
    When I search for the term Panda
    Then I see there is a search result with the text Giant panda

  @search2
  Scenario Outline: Perform a google search for multiple terms using Scenario Outline
    Given I navigate to the URL https://www.google.com
    When I search for the term <searchTerm>
    Then I see there is a search result with the text <expectedResult>
    Examples:
      | searchTerm | expectedResult                         |
      | Panda      | Giant panda                            |
      | origami    | Easy Origami Instructions and Diagrams |