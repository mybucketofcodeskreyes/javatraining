package nz.co.enhance.StepDefs.Examples;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.Examples.GoogleSearchPage;
import nz.co.enhance.automationFramework.Automator;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertTrue;

public class GoogleStepDefs {

    @Given("^I navigate to the URL (.*)$")
    public void i_navigate_to_the_URL(String url) {
        Automator.driver.navigate().to(url);
    }

    @When("^I search for the term (.*)$")
    public void iSearchForTheTerm(String searchTerm) {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchBox.sendKeys(searchTerm);
        googleSearchPage.searchBox.sendKeys(Keys.ENTER);
    }

    @Then("^I see there is a search result with the text (.*)$")
    public void iSeeThereIsASearchResultWithTheText(String text) {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();
        googleSearchPage.searchResults.waitForElementToBeDisplayed(10); //make sure the search results are fully loaded
        assertTrue(googleSearchPage.searchResults.getText().contains(text));
    }
}
