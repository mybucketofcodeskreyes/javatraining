package nz.co.enhance.StepDefs.Examples;

import cucumber.api.java.en.Then;
import nz.co.enhance.Objects.Examples.NZHeraldPage;

import static org.junit.Assert.assertTrue;

public class MobileStepDefs {

    @Then("^I see the NZ Herald home icon is visible$")
    public void iSeeTheNZHeraldHomeIconIsVisible() {
        NZHeraldPage nzh = new NZHeraldPage();
        assertTrue("The home icon should be displayed", nzh.homeButton.isDisplayed());
    }
}
