package nz.co.enhance.StepDefs.Examples;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nz.co.enhance.Objects.Examples.PostBody;
import nz.co.enhance.StepDefs.Hooks;
import nz.co.enhance.automationFramework.HelperClasses.JSONParser;
import nz.co.enhance.automationFramework.ServiceClasses.GETRequest;
import nz.co.enhance.automationFramework.ServiceClasses.HTTPRequest;
import nz.co.enhance.automationFramework.ServiceClasses.POSTRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class APIStepDefs {

    HTTPRequest request;

    @When("^I send a GET to the test api for user (.*)$")
    public void iSendAGETToTheTestApiForUser(String user) {
        String endpoint = "https://jsonplaceholder.typicode.com/todos/" + user;
        request = new GETRequest(endpoint, null);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    @Then("^I see the response code is (\\d+)$")
    public void iSeeTheResponseCodeIs(int code) {
        assertEquals(code, request.responseCode);
    }

    @And("^I see the response contains the text (.*)$")
    public void iSeeTheResponseContainsTheText(String text) {
        assertTrue(request.response.contains(text));
    }

    @And("^I see the response contains the following data:$")
    public void iSeeTheResponseContainsTheFollowingData(DataTable dataTable) {
        String results = "";
        boolean result = true;

        List<List<String>> data = dataTable.cells(1); //turn the table into a list, ignoring the first row

        JSONParser j = new JSONParser(request.response);
        for (List<String> pairToCheck : data) {
            String field = pairToCheck.get(0);
            String value = pairToCheck.get(1);
            String actualValue = j.findFirstValueOnPath(field);
            if (actualValue.equalsIgnoreCase(value)) {
                results += "PASSED: Field: " + field + " had the value " + value + " \n";
            } else {
                results += "FAILED: Field: " + field + " expected the value " + value + " but actually had: " + actualValue + "\n";
                result = false;
            }
        }

        Hooks.scenario.write(results);
        assertTrue(result);
    }

    @Given("^I send a POST request to the test api for user (\\d+)$")
    public void iSendAPOSTRequestToTheTestApiForUser(int user) {
        String endpoint = "https://jsonplaceholder.typicode.com/posts";

        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("Content-Type", "application/json;"));

        String body = "{\n" +
                "    \"userId\": " + user + ",\n" +
                "    \"title\": \"Wibble\",\n" +
                "    \"body\": \"Wobble\"\n" +
                "  }";

        request = new POSTRequest(endpoint, body, headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());
    }

    @When("^I send a POST request with the following data:$")
    public void iSendAPOSTRequestWithTheFollowingData(DataTable dataTable) {
        String endpoint = "https://jsonplaceholder.typicode.com/posts";

        List<List<String>> headers = new ArrayList<>();
        headers.add(Arrays.asList("Content-Type", "application/json;"));

        Map<String, String> data = dataTable.asMap(String.class, String.class);

        PostBody postBody = new PostBody();
        postBody.userId = Integer.parseInt(data.get("userId"));
        postBody.title = data.get("title");
        postBody.body = data.get("body");

        request = new POSTRequest(endpoint, postBody.toJson(), headers);
        request.sendRequest();
        Hooks.scenario.write(request.logRequestAndResponse());


    }
}
