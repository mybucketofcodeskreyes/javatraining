package nz.co.enhance;
/**
 * This class controls the Cucumber setup and tags.
 */

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import nz.co.enhance.automationFramework.ReportingClasses.SpiderReport.SpiderReportingData;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;


@RunWith(Cucumber.class)
@CucumberOptions(
        //This specifies the type and location of your desired reports.
        plugin = {"pretty", "nz.co.enhance.automationFramework.ReportingClasses.HTML:target/cucumberHTML", "json:target/cucumber.json", "junit:target/cucumber-junit.xml"},
        //This is the location of your Features folder
        features = {"src/test/java/nz/co/enhance/Features"},
        //This is the location of your StepDefs folder
        glue = {"nz.co.enhance.StepDefs"},
        //This is an optional flag that lets all tests that match the tags be checked without executing them. Use it to
        //dryRun = true,
        //These are the tags that will be executed if you run this class.
        tags = {"@search"})

public class TestRunner {
    //Here is where you can put @BeforeAll and @AfterAll methods to run before and after the suite executes.
    public static List<SpiderReportingData> data = new ArrayList<>();
}
//ghg-to-bones-bones
//sarap-tsarap-tsarap
//test 1234567890